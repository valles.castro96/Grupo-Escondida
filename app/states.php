<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class states extends Model
{
    //
    public function cities()
    {
        return $this->hasMany('App\City','city_id');
    }
}
