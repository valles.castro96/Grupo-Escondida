/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost
 Source Database       : chuliprecio

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : utf-8

 Date: 01/22/2020 16:27:00 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `states`
-- ----------------------------
DROP TABLE IF EXISTS `states`;
CREATE TABLE `states` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `states_country_id_foreign` (`country_id`),
  CONSTRAINT `states_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
--  Records of `states`
-- ----------------------------
BEGIN;
INSERT INTO `states` VALUES ('1', 'Aguascalientes', '1', null, null), ('2', 'Baja California', '1', null, null), ('3', 'Baja California Sur', '1', null, null), ('4', 'Campeche', '1', null, null), ('5', 'Cd. de México', '1', null, null), ('6', 'Chiapas', '1', null, null), ('7', 'Chihuahua', '1', null, null), ('8', 'Coahuila de Zaragoza', '1', null, null), ('9', 'Colima', '1', null, null), ('10', 'Durango', '1', null, null), ('11', 'Estado de México', '1', null, null), ('12', 'Guanajuato', '1', null, null), ('13', 'Guerrero', '1', null, null), ('14', 'Hidalgo', '1', null, null), ('15', 'Jalisco', '1', null, null), ('16', 'Michoacán de Ocampo', '1', null, null), ('17', 'Morelos', '1', null, null), ('18', 'Nayarit', '1', null, null), ('19', 'Nuevo León', '1', null, null), ('20', 'Oaxaca', '1', null, null), ('21', 'Puebla', '1', null, null), ('22', 'Querétaro', '1', null, null), ('23', 'Quintana Roo', '1', null, null), ('24', 'San Luis Potosí', '1', null, null), ('25', 'Sinaloa', '1', null, null), ('26', 'Sonora', '1', null, null), ('27', 'Tabasco', '1', null, null), ('28', 'Tamaulipas', '1', null, null), ('29', 'Tlaxcala', '1', null, null), ('30', 'Veracruz de Ignacio de la Llave', '1', null, null), ('31', 'Yucatán', '1', null, null), ('32', 'Zacatecas', '1', null, null);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
