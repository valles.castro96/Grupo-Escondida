<?php

use Illuminate\Database\Seeder;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categorias = 
        [
            [
                'categoria' => 'Cabañas',
                'estado_id' => 10,
                'municipio_id' => 286
            ],
            [
                'categoria' => 'Departamentos',
                'estado_id' => 25,
                'municipio_id' => 1876
            ]
        ];

        foreach($categorias as $categoria){
            $insert = DB::table('categorias')->insert($categoria);
            if($insert){
                echo "Creado ".$categoria['categoria']."\n";
            }else{
                echo "error ".$categoria['categoria']."\n";
            }
        }
    }
}
