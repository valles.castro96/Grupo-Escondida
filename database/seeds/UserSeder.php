<?php

use Illuminate\Database\Seeder;

class UserSeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'     => 'Root',
			'email'    => 'root@root.com',
			'password' => bcrypt('12345678'),
        ]);
    }
}
